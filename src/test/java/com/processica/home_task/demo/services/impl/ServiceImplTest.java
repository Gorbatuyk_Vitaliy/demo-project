package com.processica.home_task.demo.services.impl;

import com.processica.home_task.demo.model.Request;
import com.processica.home_task.demo.repository.RepositoryRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ServiceImplTest {
    private final String  REQUEST_WORD_TEST= "I love Java, hello Java";

    @InjectMocks
    private ServiceImpl service;

    @Mock
    private RepositoryRequest userRepository;

    @Test
    void searchNumberOfUniqueWords() {

    }

    private HashMap<String,Integer> createMapUniqueWordsTest() {
        HashMap<String, Integer> mapUniqueWords = new HashMap<>();
        mapUniqueWords.put("I",1);
        mapUniqueWords.put("love",1);
        mapUniqueWords.put("Java",2);
        mapUniqueWords.put("hello",1);
        return mapUniqueWords;
    }
}