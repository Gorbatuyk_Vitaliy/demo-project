package com.processica.home_task.demo.exeption;

import com.processica.home_task.demo.model.CustomError;
import lombok.extern.log4j.Log4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


@RestControllerAdvice
@Log4j
public class GlobalExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FOUND)
    public CustomError exceptionHandler(TextIsPresent e) {
        log.error(e.getMessage());
        return new CustomError(e.getMessage(), LocalDateTime.now());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public List<CustomError> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.error(ex.getMessage());
        return ex.getBindingResult().getAllErrors().stream()
                .map(err -> new CustomError(
                        err.getDefaultMessage(), LocalDateTime.now()))
                .collect(Collectors.toList());
    }
}
