package com.processica.home_task.demo.exeption;

public class TextIsPresent extends RuntimeException{
    public TextIsPresent(String message) {
        super(message);
    }
}
