package com.processica.home_task.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatsDto {

    private String word;
    private int entry;

    @Override
    public String toString() {
        return "{'"
                + word +
                "' = " + entry +
                '}';
    }
}
