package com.processica.home_task.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "request")
public class Request {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "pay_load", unique = true)
    private String payLoad;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "request")
    private List<Stats> stats;

    @Override
    public String toString() {
        return "Request{" +
                "id=" + id +
                ", payLoad='" + payLoad + '\'' +
                '}';
    }
}
