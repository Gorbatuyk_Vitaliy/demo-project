package com.processica.home_task.demo.controller;

import com.processica.home_task.demo.model.dto.RequestDto;
import com.processica.home_task.demo.services.ServiceApp;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;


@RestController
@RequestMapping("/processed")
@RequiredArgsConstructor
public class Controller {

    private final ServiceApp serviceApp;

    @PostMapping("/count_unique_word")
    public long showUniqueWords(@RequestBody @Valid RequestDto requestDto ) {
        return serviceApp.showUniqueWords(requestDto);
    }
@PostMapping("/all_statistic")
    public HashMap<String, Integer> showAllStats(@RequestBody @Valid RequestDto requestDto){
        return serviceApp.showAllStats(requestDto);
    }
}
