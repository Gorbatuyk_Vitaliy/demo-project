package com.processica.home_task.demo.services;

import com.processica.home_task.demo.model.dto.RequestDto;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface ServiceApp {
    long showUniqueWords(RequestDto incomingStr);

    HashMap<String, Integer> showAllStats(RequestDto requestDto);
}
