package com.processica.home_task.demo.services.impl;

import com.processica.home_task.demo.exeption.TextIsPresent;
import com.processica.home_task.demo.mapper.RequestMapper;
import com.processica.home_task.demo.model.Request;
import com.processica.home_task.demo.model.Stats;
import com.processica.home_task.demo.model.dto.RequestDto;
import com.processica.home_task.demo.repository.RepositoryRequest;
import com.processica.home_task.demo.repository.RepositoryStats;
import com.processica.home_task.demo.services.ServiceApp;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class ServiceImpl implements ServiceApp {
    private final RepositoryRequest repositoryRequest;
    private final RepositoryStats repositoryStats;

    @Override
    public long showUniqueWords(RequestDto requestDto) {
        requestDto.setPayLoad(requestDto.getPayLoad().trim());
        Request request = RequestMapper.INSTANCE.mapToRequest(requestDto);
        checkIfExistSentenceFromData(request);
        request.setStats(setStatsToList(request));
        repositoryRequest.save(request);
        return request.getStats().stream()
                .filter(stats -> stats.getEntry() == 1)
                .count();
    }

    @Override
    public HashMap<String, Integer> showAllStats(RequestDto requestDto) {
        requestDto.setPayLoad(requestDto.getPayLoad().trim());
        Request request = RequestMapper.INSTANCE.mapToRequest(requestDto);
        request = checkExistSentenceFromData(request);
       List<Stats> statsList = repositoryStats.findAllByRequest(request,
               Sort.by(Sort.Direction.ASC, "word"));
   //     List<Stats> statsList = repositoryStats.findByRequestOrderByWordDesc(request);
        return setStatsToMap(statsList);
    }

    private HashMap<String, Integer> setStatsToMap(List<Stats> statsList) {
        HashMap<String, Integer> mapStats = new HashMap<>();
        for (Stats s : statsList) {
            if (s.getEntry() > 1) {
                mapStats.put(s.getWord(), s.getEntry());
            }
        }
        return mapStats;
    }

    private Request checkExistSentenceFromData(Request request) {
        return Optional.ofNullable(repositoryRequest.getByPayLoad(request.getPayLoad().trim()))
                .orElseThrow(() -> new TextIsPresent("This text is missing from the database"));
    }

    private void checkIfExistSentenceFromData(Request request) {
        if (repositoryRequest.getByPayLoad(request.getPayLoad().trim()) != null)
            throw new TextIsPresent("This text is present in date base!");
    }

    protected List<Stats> setStatsToList(Request request) {
        HashMap<String, Integer> uniqueWords = searchNumberOfUniqueWords(request);
        List<Stats> statsList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : uniqueWords.entrySet()) {
            statsList.add(new Stats(entry.getKey(), entry.getValue(), request));
        }
        return statsList;
    }

    private HashMap<String, Integer> searchNumberOfUniqueWords(Request str) {
        String incomingStr = str.getPayLoad();
        String[] splitWords = incomingStr.split("[^\\w]");
        HashMap<String, Integer> uniquesWords = new HashMap<>();

        for (String word : splitWords) {
            if (uniquesWords.containsKey(word)) {
                int count = uniquesWords.get(word);
                uniquesWords.put(word, count + 1);
            } else {
                uniquesWords.put(word, 1);
            }
        }
        return uniquesWords;
    }
}
