package com.processica.home_task.demo.repository;

import com.processica.home_task.demo.model.Request;
import com.processica.home_task.demo.model.Stats;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RepositoryStats extends JpaRepository<Stats, Long> {
    List<Stats> findAllByRequest(Request request, Sort sort);
    //List<Stats> findByRequestOrderByWordDesc(Request request);
}
