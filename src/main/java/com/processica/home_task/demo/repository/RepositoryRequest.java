package com.processica.home_task.demo.repository;

import com.processica.home_task.demo.model.Request;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RepositoryRequest extends JpaRepository<Request, Long> {
    Request getByPayLoad(String sentence);
}
