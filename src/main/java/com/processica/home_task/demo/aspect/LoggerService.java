package com.processica.home_task.demo.aspect;

import com.processica.home_task.demo.model.dto.StatsDto;
import lombok.extern.log4j.Log4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@Aspect
@Log4j
public class LoggerService {

    @AfterReturning(pointcut = "execution(* com.processica.home_task.demo.services.impl.ServiceImpl.show*(..))",
            returning = "statsList")
    public void writeLogForResultMethodParseStringAdvice(JoinPoint joinPoint, List<Object> statsList) {
        log.info("Input text " + "\"" + Arrays.toString(joinPoint.getArgs()) + "\" has: "
                + statsList);
    }
}
