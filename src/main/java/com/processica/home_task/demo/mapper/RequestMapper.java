package com.processica.home_task.demo.mapper;

import com.processica.home_task.demo.model.Request;
import com.processica.home_task.demo.model.dto.RequestDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RequestMapper {
    RequestMapper INSTANCE = Mappers.getMapper(RequestMapper.class);

    RequestDto mapToRequestDto(Request request);
    Request mapToRequest(RequestDto requestDto);
}
