package com.processica.home_task.demo.mapper;

import com.processica.home_task.demo.model.Stats;
import com.processica.home_task.demo.model.dto.StatsDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface StatsMapper {
    StatsMapper INSTANCE = Mappers.getMapper(StatsMapper.class);

    StatsDto mapToStatsDto(Stats stats);
    Stats mapToStats(StatsDto statsDto);

}
